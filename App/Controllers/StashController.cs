﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace App.Controllers
{
    public class StashController : ApiController
    {
        // GET api/values
        public string Get()
        {
            string sline="";
            try
            {   // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader("C:\\source\\TestFile.txt"))
                {
                    // Read the stream to a string, and write the string to the console.
                     sline = sr.ReadToEnd();
                    Console.WriteLine(sline);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }
            return sline;
        }

        // GET api/values/5
        public string Get(int id)
        {
            string new_value = "new_value";

            using (StreamWriter outputFile = new StreamWriter("C:\\source\\TestFile.txt", false))
            {
                outputFile.Write(new_value);
            }
            return new_value;
        }

        public void AddValue(string new_value)
        {
            using (StreamWriter outputFile = new StreamWriter("C:\\source\\TestFile.txt", false))
            {
                outputFile.Write(new_value);
            }
        }
        // POST api/values
        public void Post([FromBody]string new_value) {
            using (StreamWriter outputFile = new StreamWriter("C:\\source\\TestFile.txt", false))
            {
                outputFile.Write(new_value);
            }
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
