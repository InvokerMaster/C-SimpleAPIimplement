﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Xml.Linq;

namespace App.Models
{
    public class APIModel
    {
        public HttpClientHandler m_handler;
        public HttpClient m_client;
        public string m_apikey = "";
        
        public APIModel()
        {
            m_handler = new HttpClientHandler()
            {
                UseDefaultCredentials = true
            };
        }
        public void addValue(string pValue)
        {
            using (m_client = new HttpClient())
            {
                var response = m_client.PostAsJsonAsync("http://127.0.0.1/api/Stash/AddValue", pValue);
                response.Wait();
                var responseString = response.Result.Content.ReadAsStringAsync();
            }
        }
        public string getValue()
        {
            using (m_client = new HttpClient())
            {
                m_client.BaseAddress = new Uri("http://127.0.0.1/api/");
                var result = m_client.GetAsync("Stash");
                result.Wait();
                return result.Result.Content.ReadAsStringAsync().Result;
            }
        }
    }
}